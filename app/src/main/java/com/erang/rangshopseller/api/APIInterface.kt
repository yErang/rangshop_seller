package com.erang.rangshopseller.api

import com.erang.rangshopseller.api.response.AccountListResponse
import com.erang.rangshopseller.api.response.ProductResponse
import com.erang.rangshopseller.api.response.ResultResponse
import com.erang.rangshopseller.api.response.SellingListResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APIInterface {

    //제품 관리

    @Multipart
    @POST("management/product/uploadProduct.php")
    fun uploadProduct(@Part file : MultipartBody.Part,@PartMap partmap : HashMap<String, RequestBody>) : Call<ResultResponse>


    @GET("management/product/listSelling.php")
    fun getSellingList(@Query("category") category: String) :Call<List<SellingListResponse>>


    @GET("management/product/productView.php")
    fun getProduct(@Query("product_num") product_num: String) :Call<List<ProductResponse>>

    @Multipart
    @POST("management/product/updateProduct.php")
    fun updateProduct(@Part file : MultipartBody.Part? ,@PartMap partmap : HashMap<String, RequestBody>) : Call<ResultResponse>

    @FormUrlEncoded
    @POST("management/product/deleteProduct.php")
    fun deleteProduct(@Field("product_num") product_num: String) :Call<ResultResponse>


    // 사용자 관리

    @GET("management/account/listAccount.php")
    fun getAccountList() : Call<List<AccountListResponse>>
}