package com.erang.rangshopseller.api.response

import com.google.gson.annotations.SerializedName

class AccountListResponse{

    @SerializedName("account_num")
    var num : String? = null

    @SerializedName("email")
    var email : String? = null

    @SerializedName("name")
    var name : String? = null

    @SerializedName("order_count")
    var order : String? = null

    @SerializedName("register_time")
    var date : String? = null

    @SerializedName("address")
    var address : String? = null
}