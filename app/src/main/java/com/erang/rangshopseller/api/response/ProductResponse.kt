package com.erang.rangshopseller.api.response

import com.google.gson.annotations.SerializedName

class ProductResponse{


    @SerializedName("product_img")
    var img : String? = null

    @SerializedName("product_name")
    var name : String? = null

    @SerializedName("product_price")
    var price : String? = null

    @SerializedName("product_quantity")
    var quantity : String? = null

    @SerializedName("product_desc")
    var desc : String? = null

    @SerializedName("product_category")
    var category : String? = null

    @SerializedName("product_purity")
    var purity : String? = null
}