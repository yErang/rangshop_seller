package com.erang.rangshopseller.api.response

import com.google.gson.annotations.SerializedName

class SellingListResponse {

    @SerializedName("product_num")
    var num : String? = null

    @SerializedName("product_img")
    var img : String? = null

    @SerializedName("product_name")
    var name : String? = null

    @SerializedName("product_price")
    var price : String? = null

    @SerializedName("product_quantity")
    val quantity :String? = null

    @SerializedName("product_purity")
    val purity :String? = null
}