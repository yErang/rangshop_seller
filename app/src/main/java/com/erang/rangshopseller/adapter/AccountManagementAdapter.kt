package com.erang.rangshopseller.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.erang.rangshopseller.R
import com.erang.rangshopseller.activity.management.AccountDatailView
import com.erang.rangshopseller.api.response.AccountListResponse
import kotlinx.android.synthetic.main.activity_management_account_item.view.*

class AccountManagementAdapter(var items : List<AccountListResponse>, val context: Context) : RecyclerView.Adapter<AccountManagementAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountManagementAdapter.ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.activity_management_account_item,parent,false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: AccountManagementAdapter.ViewHolder, position: Int) {

        holder.accountNum.text = items[position].num
        holder.accountEmail.text = items[position].email
        holder.accountName.text = items[position].name
        holder.accountOrder.text = items[position].order

        val accountDate = items[position].date
        val accountAddress = items[position].address

        holder.itemView.setOnClickListener {
            val intent = Intent(context, AccountDatailView::class.java)
            intent.putExtra("num",holder.accountNum.text.toString())
            intent.putExtra("email",holder.accountEmail.text.toString())
            intent.putExtra("name",holder.accountName.text.toString())
            intent.putExtra("order",holder.accountOrder.text.toString())
            intent.putExtra("address",accountAddress)
            intent.putExtra("date",accountDate)
            context.startActivity(intent)
        }

    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val accountEmail = view.management_account_email
        val accountName = view.management_account_name
        val accountOrder = view.management_account_order
        val accountNum = view.management_account_num
    }
}