package com.erang.rangshopseller.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.erang.rangshopseller.R
import com.erang.rangshopseller.activity.product.ProductViewActivity
import com.erang.rangshopseller.api.response.SellingListResponse
import com.erang.rangshopseller.util.TextUtil
import kotlinx.android.synthetic.main.fragment_selling_item.view.*

class SellingAdapter(var items : List<SellingListResponse>, val context: Context) : RecyclerView.Adapter<SellingAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.fragment_selling_item,parent,false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(items[position].img).thumbnail(0.1f).apply(RequestOptions.noTransformation()).into(holder.productImg)
        holder.productName.text = items[position].name
        holder.productPrice.text = TextUtil.setPrice(items[position].price) +"원"
        if(items[position].quantity?.toInt() == 0){
            holder.productSoldout.visibility = View.VISIBLE
        } else {
            holder.productSoldout.visibility = View.GONE
        }

        holder.productPurity.text = items[position].purity
        val productNum = items[position].num
        holder.itemView.setOnClickListener {
            val intent = Intent(context, ProductViewActivity::class.java)
            intent.putExtra("product_num",productNum)
            context.startActivity(intent)
        }

    }


    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val productImg = view.sell_list_img
        val productSoldout = view.sell_list_soldout
        val productName = view.sell_list_name
        val productPrice = view.sell_list_price
        val productPurity = view.sell_list_purity


    }
}