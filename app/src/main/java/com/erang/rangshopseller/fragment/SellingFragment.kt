package com.erang.rangshopseller.fragment


import android.os.Bundle
import android.app.Fragment
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.erang.rangshopseller.R
import com.erang.rangshopseller.adapter.SellingAdapter
import com.erang.rangshopseller.api.response.SellingListResponse
import com.erang.rangshopseller.util.Common
import kotlinx.android.synthetic.main.fragment_selling.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/* 판매 목록 페이지 */

class SellingFragment : Fragment() {

    lateinit var sellingView : View
    lateinit var sellingRecyclerView :RecyclerView
    lateinit var sellingList : List<SellingListResponse>

    lateinit var menuTab : TabLayout
    val category =arrayOf("전체","반지","목걸이","귀걸이","팔찌")

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        sellingView = inflater.inflate(R.layout.fragment_selling, container, false)
        init()

        setOnClick()
        getData(category[0])

        return sellingView
    }

    fun init(){
        sellingList = ArrayList()
        sellingRecyclerView = sellingView.sellList
        sellingRecyclerView.layoutManager = LinearLayoutManager(activity)

        menuTab = sellingView.sell_list_tab

    }

    fun getData(category : String){
        val call = Common.api.getSellingList(category = category)
        call.enqueue(object : Callback<List<SellingListResponse>>{
            override fun onResponse(call: Call<List<SellingListResponse>>, response: Response<List<SellingListResponse>>?) {

                sellingList = response?.body()!!
                sellingRecyclerView.adapter = SellingAdapter(sellingList,activity)
                sellingRecyclerView.adapter.notifyDataSetChanged()

            }
            override fun onFailure(call: Call<List<SellingListResponse>>?, t: Throwable?) {
            }

        })
    }
    fun setOnClick(){


        menuTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position){
                    0 -> getData(category[0])
                    1 -> getData(category[1])
                    2 -> getData(category[2])
                    3 -> getData(category[3])
                    4 -> getData(category[4])
                }
            }

        })

    }



}
