package com.erang.rangshopseller.fragment

import android.os.Bundle
import android.app.Fragment
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.erang.rangshopseller.R
import com.erang.rangshopseller.activity.management.AccountManagementActivity
import kotlinx.android.synthetic.main.fragment_management.*
import kotlinx.android.synthetic.main.fragment_management.view.*

/* 관리 페이지 */

class ManagementFragment : Fragment() {

    lateinit var managementView : View

    lateinit var managementAccount : Button
    lateinit var managementOrder : Button


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        managementView= inflater.inflate(R.layout.fragment_management, container, false)

        init()
        setOnClick()

        return managementView
    }

    fun init(){
        managementAccount = managementView.management_account
        managementOrder = managementView.management_order
    }

    fun setOnClick(){
        managementAccount.setOnClickListener {
            val intent = Intent(activity,AccountManagementActivity::class.java)
            startActivity(intent)
        }
        managementOrder.setOnClickListener {

        }
    }
}
