package com.erang.rangshopseller.fragment


import android.app.Activity
import android.os.Bundle
import android.app.Fragment
import android.content.Intent
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.erang.rangshopseller.R
import com.erang.rangshopseller.api.response.ResultResponse
import com.erang.rangshopseller.util.Common
import com.erang.rangshopseller.util.TextUtil
import kotlinx.android.synthetic.main.fragment_upload.view.*
import okhttp3.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

/* 상품 등록 페이지 */

class UploadFragment : Fragment() {

    lateinit var uploadView:View
    lateinit var uploadName : EditText
    lateinit var uploadPrice : EditText
    lateinit var uploadQuantity : EditText
    lateinit var uploadDesc : EditText
    lateinit var btnUpload : Button
    lateinit var uploadImage : ImageView
    val IMG_REQUEST = 100
    lateinit var imagePath : String
    lateinit var uploadCategory : Spinner
    val categoryArray = arrayOf("카테고리","반지","목걸이","귀걸이","팔찌")
    lateinit var selectedCategory: String
    var isImageSelected = false
    var isCategorySelected = false
    var isPuritySelected = false
    var textPurity : String?=null
    lateinit var uploadPurity : RadioGroup



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        uploadView = inflater.inflate(R.layout.fragment_upload, container, false)
        init()
        setOnClick()
        selectCategory()

        return uploadView
    }

    fun init(){
        uploadName = uploadView.upload_name
        uploadPrice = uploadView.upload_price
        uploadDesc = uploadView.upload_desc
        uploadQuantity = uploadView.upload_quantity
        uploadImage = uploadView.upload_img
        btnUpload = uploadView.uploadBtn
        uploadCategory = uploadView.upload_category
        uploadCategory.adapter=ArrayAdapter(activity,android.R.layout.simple_spinner_dropdown_item,categoryArray)
        uploadPurity = uploadView.upload_purity

    }

    fun setOnClick(){
        uploadImage.setOnClickListener {
            selectImage()
        }
        btnUpload.setOnClickListener {

            if(TextUtil.isEmpty(uploadName)){
                Toast.makeText(activity,"상품명을 입력 해 주세요",Toast.LENGTH_SHORT).show()
                uploadName.requestFocus()
            } else if (TextUtil.isEmpty(uploadPrice)){
                Toast.makeText(activity,"가격을 입력 해 주세요",Toast.LENGTH_SHORT).show()
                uploadPrice.requestFocus()
            } else if (TextUtil.isEmpty(uploadQuantity)){
                Toast.makeText(activity,"상품 수량을 입력 해 주세요",Toast.LENGTH_SHORT).show()
                uploadQuantity.requestFocus()
            } else if (!isImageSelected) {
                Toast.makeText(activity,"사진을 선택 해 주세요",Toast.LENGTH_SHORT).show()
            } else if  (!isCategorySelected){
                Toast.makeText(activity,"카테고리를 선택 해 주세요",Toast.LENGTH_SHORT).show()
            } else if (!isPuritySelected) {
                Toast.makeText(activity,"은 / 금 순도를 선택 해 주세요",Toast.LENGTH_SHORT).show()
            } else {
                uploadFile()
            }
        }

        uploadPurity.setOnCheckedChangeListener{ radioGroup, i ->
            isPuritySelected = true
            val radio : RadioButton = radioGroup.findViewById(i)
            textPurity = radio.text.toString()
        }

    }
    fun selectImage(){
        val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
       startActivityForResult(galleryIntent, IMG_REQUEST)
    }

    fun selectCategory(){
        uploadCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position==0){
                    isCategorySelected = false
                } else {
                    selectedCategory = categoryArray[position]
                    isCategorySelected = true
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try{
            if(requestCode==IMG_REQUEST&&resultCode== Activity.RESULT_OK&&data!=null){

                var imageUri=data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = activity.contentResolver.query(imageUri!!, filePathColumn, null, null, null)
                cursor!!.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                imagePath = cursor.getString(columnIndex)

                uploadImage.setImageBitmap(BitmapFactory.decodeFile(imagePath))
                cursor.close()
                isImageSelected = true
            } else {
                Toast.makeText(activity,"사진을 선택하지 않았습니다",Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception){
            Toast.makeText(activity,"오류가 발생했습니다.",Toast.LENGTH_SHORT).show()
        }
    }

    fun uploadFile() {

        val file = File(imagePath)
        val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
        val imageToUpload = MultipartBody.Part.createFormData("file", file.name, requestBody)

        val productCategory = RequestBody.create(MediaType.parse("text/plain"), selectedCategory)
        val productName = RequestBody.create(MediaType.parse("text/plain"), uploadName.text.toString())
        val productPrice = RequestBody.create(MediaType.parse("text/plain"), uploadPrice.text.toString())
        val productQuantity = RequestBody.create(MediaType.parse("text/plain"), uploadQuantity.text.toString())
        val productDesc = RequestBody.create(MediaType.parse("text/plain"), uploadDesc.text.toString())
        val productPurity = RequestBody.create(MediaType.parse("text/plain"), textPurity)

        var map = HashMap<String, RequestBody>()
        map.put("product_name", productName)
        map.put("product_price", productPrice)
        map.put("product_quantity", productQuantity)
        map.put("product_desc", productDesc)
        map.put("product_category",productCategory)
        map.put("product_purity",productPurity)


        val call = Common.api.uploadProduct(imageToUpload, map)
        call.enqueue(object : Callback<ResultResponse>{
            override fun onResponse(call: Call<ResultResponse>?, response: Response<ResultResponse>?) {

                if(response!!.body()!!.success){
                    Toast.makeText(activity,"상품을 업로드 했습니다.",Toast.LENGTH_SHORT).show()
                    loadFragment(SellingFragment())

                } else {
                    Toast.makeText(activity, response!!.body()?.message.toString(), Toast.LENGTH_SHORT).show()
                }
            }
            override fun onFailure(call: Call<ResultResponse>?, t: Throwable?) {
            }

        })
    }

    fun loadFragment(fragment: Fragment) : Boolean{
        fragmentManager.beginTransaction().replace(R.id.main_content,fragment).commit()
        return true
    }

}
