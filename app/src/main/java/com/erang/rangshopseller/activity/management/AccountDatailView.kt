package com.erang.rangshopseller.activity.management

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.erang.rangshopseller.R
import kotlinx.android.synthetic.main.activity_management_account_datail.*

class AccountDatailView : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_management_account_datail)

        init()


    }

    fun init(){

        val tvAccountNum : TextView = account_detail_num
        val tvAccountEmail : TextView = account_detail_email
        val tvAccountName : TextView = account_detail_name
        val tvAccountOrder: TextView = account_detail_ordercount
        val tvAccountAddress : TextView = account_detail_address
        val tvAccountDate: TextView = account_detail_registertime

        tvAccountNum.text = intent.getStringExtra("num")
        tvAccountEmail.text= intent.getStringExtra("email")
        tvAccountName.text= intent.getStringExtra("name")
        tvAccountOrder.text= intent.getStringExtra("order")
        tvAccountAddress.text= intent.getStringExtra("address")
        tvAccountDate.text= intent.getStringExtra("date")
    }
}
