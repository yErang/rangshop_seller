package com.erang.rangshopseller.activity


import android.Manifest
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.app.Fragment
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat
import com.erang.rangshopseller.R
import com.erang.rangshopseller.fragment.ManagementFragment
import com.erang.rangshopseller.fragment.SellingFragment
import com.erang.rangshopseller.fragment.UploadFragment
import com.erang.rangshopseller.util.BackPressedHandler


class MainActivity : AppCompatActivity() {
    lateinit var backPressedHandler: BackPressedHandler
    lateinit var bottomNavigationView : BottomNavigationView
    lateinit var appData : SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        bottomNavigationView.setOnNavigationItemSelectedListener { menu ->
            when(menu.itemId) {
                R.id.selling -> {loadFragment(SellingFragment())
                }
                R.id.upload -> {loadFragment(UploadFragment())
                }
                R.id.management -> { loadFragment(ManagementFragment())
                }
                else -> {
                    return@setOnNavigationItemSelectedListener false
                }
            }
        }
    }

    fun init(){
        bottomNavigationView = findViewById(R.id.main_bottom_nav)
        appData = getSharedPreferences("appData",Context.MODE_PRIVATE)
        loadFragment(SellingFragment())
        backPressedHandler = BackPressedHandler(this)
    }

    fun loadFragment(fragment: Fragment) : Boolean{
        fragmentManager.beginTransaction().replace(R.id.main_content,fragment).commit()
        return true
    }

    override fun onBackPressed() {
        backPressedHandler.onBackPressed()
    }

}
