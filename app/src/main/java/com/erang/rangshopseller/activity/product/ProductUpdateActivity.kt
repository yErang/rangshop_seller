package com.erang.rangshopseller.activity.product

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.*
import com.bumptech.glide.Glide
import com.erang.rangshopseller.R
import com.erang.rangshopseller.api.response.ResultResponse
import com.erang.rangshopseller.util.Common
import kotlinx.android.synthetic.main.activity_product_update.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProductUpdateActivity : AppCompatActivity() {

    lateinit var updateName : EditText
    lateinit var updatePrice : EditText
    lateinit var updateQuantity : EditText


    lateinit var updateImg : ImageView
    lateinit var updateDesc : EditText
    lateinit var updateFinish : Button
    lateinit var updateCancel : Button

    lateinit var updatePurity : RadioGroup
    var textPurity : String? = null
    var isPuritySelected : Boolean = false

    lateinit var updateCategory : Spinner
    var isCategorySelected : Boolean = false
    lateinit var selectedCategory : String

    var imageToUpdate : MultipartBody.Part? = null
    lateinit var imagePath : String
    var isImageUpdate : Boolean = false
    val IMG_REQUEST = 100


    val categoryArray = arrayOf("카테고리","반지","목걸이","귀걸이","팔찌")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_update)

        init()
        getIntentData()
        setOnClick()
        selectCategory()

    }

    fun init(){
        updateName = update_name
        updatePrice = update_price
        updateQuantity = update_quantity

        updateCategory = update_category
        updateCategory.adapter = ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,categoryArray)

        updatePurity = update_purity
        updateImg = update_img
        updateDesc = update_desc

        updateFinish = update_finish
        updateCancel = update_cancel


    }

    fun getIntentData(){

        updateName.setText(intent.getStringExtra("product_name"))

        updatePrice.setText(intent.getStringExtra("product_price"))
        updateQuantity.setText(intent.getStringExtra("product_quantity"))

        when(intent.getStringExtra("product_category")){
            "반지" -> updateCategory.setSelection(1)
            "목걸이" -> updateCategory.setSelection(2)
            "귀걸이" -> updateCategory.setSelection(3)
            "팔찌" -> updateCategory.setSelection(4)
        }

        when(intent.getStringExtra("product_purity")){
            "은" -> {
                updatePurity.check(R.id.update_rbsilver)
                textPurity = "은"
            }
            "14k" -> {
                updatePurity.check(R.id.update_rb14k)
                textPurity = "14k"
            }
            "18k" -> {
                updatePurity.check(R.id.update_rb18k)
                textPurity = "18k"
            }
            "24k" -> {
                updatePurity.check(R.id.update_rb24k)
                textPurity = "24k"
            }
        }
        Glide.with(this).load(intent.getStringExtra("product_img")).into(updateImg)
        updateDesc.setText(intent.getStringExtra("product_desc"))

    }

    fun setOnClick(){
        updateFinish.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("입력하신 정보로 수정됩니다.")
            builder.setPositiveButton("확인") { dialogInterface, i ->
                doUpdate()
            }
            builder.setNegativeButton("취소"){ dialogInterface, i ->
                dialogInterface.dismiss()
            }
            builder.create().show()
        }
        updateCancel.setOnClickListener {
            Toast.makeText(this,"상품 업데이트가 취소되었습니다.",Toast.LENGTH_SHORT).show()
            finish()
        }

        updateImg.setOnClickListener {
            val galleryIntent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(galleryIntent, IMG_REQUEST)
        }

        updatePurity.setOnCheckedChangeListener{ radioGroup, i ->
            if(updatePurity.isSelected){
                isPuritySelected = true
            }
            val radio : RadioButton = radioGroup.findViewById(i)
            textPurity = radio.text.toString()
        }
    }



    fun selectCategory(){
        updateCategory.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position==0){
                    isCategorySelected = false
                } else {
                    selectedCategory = categoryArray[position]
                    isCategorySelected = true
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try{
            if(requestCode==IMG_REQUEST&&resultCode== Activity.RESULT_OK&&data!=null){

                var imageUri=data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = this.contentResolver.query(imageUri!!, filePathColumn, null, null, null)
                cursor!!.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                imagePath = cursor.getString(columnIndex)
                updateImg.setImageBitmap(BitmapFactory.decodeFile(imagePath))
                cursor.close()
                isImageUpdate = true
            } else {
                Toast.makeText(this,"사진을 선택하지 않았습니다",Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception){
            Toast.makeText(this,"오류가 발생했습니다.",Toast.LENGTH_SHORT).show()
        }
    }

    fun doUpdate(){

        if(isImageUpdate){
            val file = File(imagePath)
            val requestBody = RequestBody.create(MediaType.parse("*/*"), file)
            imageToUpdate = MultipartBody.Part.createFormData("file", file.name, requestBody)
        }

        val productNum = RequestBody.create(MediaType.parse("text/plain"),intent.getStringExtra("product_num"))
        val productCategory = RequestBody.create(MediaType.parse("text/plain"), selectedCategory)
        val productName = RequestBody.create(MediaType.parse("text/plain"), updateName.text.toString())
        val productPrice = RequestBody.create(MediaType.parse("text/plain"), updatePrice.text.toString())
        val productQuantity = RequestBody.create(MediaType.parse("text/plain"), updateQuantity.text.toString())
        val productDesc = RequestBody.create(MediaType.parse("text/plain"), updateDesc.text.toString())
        val productPurity = RequestBody.create(MediaType.parse("text/plain"), textPurity)

        var map = HashMap<String, RequestBody>()
        map.put("product_num",productNum)
        map.put("product_name", productName)
        map.put("product_price", productPrice)
        map.put("product_quantity", productQuantity)
        map.put("product_desc", productDesc)
        map.put("product_category",productCategory)
        map.put("product_purity",productPurity)


        val call = Common.api.updateProduct(imageToUpdate, map)
        call.enqueue(object : Callback<ResultResponse> {
            override fun onResponse(call: Call<ResultResponse>?, response: Response<ResultResponse>?) {

                Toast.makeText(this@ProductUpdateActivity, response!!.body()?.message.toString(), Toast.LENGTH_SHORT).show()

                if (response!!.body()?.message.toString() == "update success"){
                    finish()
                }
            }

            override fun onFailure(call: Call<ResultResponse>?, t: Throwable?) {
            }

        })
    }

}
