package com.erang.rangshopseller.activity.management

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.erang.rangshopseller.R
import com.erang.rangshopseller.adapter.AccountManagementAdapter
import com.erang.rangshopseller.api.response.AccountListResponse
import com.erang.rangshopseller.util.Common
import kotlinx.android.synthetic.main.activity_management_account.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountManagementActivity : AppCompatActivity() {

    lateinit var accountRecyclerView : RecyclerView
    lateinit var accountList : List<AccountListResponse>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_management_account)

        init()
        getData()

    }

    fun init(){
        accountList = ArrayList()
        accountRecyclerView = management_rv
        accountRecyclerView.adapter = AccountManagementAdapter(accountList,this@AccountManagementActivity)
        accountRecyclerView.layoutManager = LinearLayoutManager(this@AccountManagementActivity)
    }

    fun getData(){
        val call = Common.api.getAccountList()
        call.enqueue(object : Callback<List<AccountListResponse>>{
            override fun onResponse(call: Call<List<AccountListResponse>>?, response: Response<List<AccountListResponse>>?) {

                accountList = response!!.body()!!
                accountRecyclerView.adapter = AccountManagementAdapter(accountList,this@AccountManagementActivity)
                accountRecyclerView.adapter.notifyDataSetChanged()
            }

            override fun onFailure(call: Call<List<AccountListResponse>>?, t: Throwable?) {
            }

        })
    }

    override fun onResume() {
        super.onResume()
        getData()
    }
}
