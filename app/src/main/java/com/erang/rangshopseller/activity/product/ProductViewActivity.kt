package com.erang.rangshopseller.activity.product

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.erang.rangshopseller.R
import com.erang.rangshopseller.api.response.ProductResponse
import com.erang.rangshopseller.api.response.ResultResponse
import com.erang.rangshopseller.util.Common
import com.erang.rangshopseller.util.TextUtil
import kotlinx.android.synthetic.main.activity_product_view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProductViewActivity : AppCompatActivity() {

    lateinit var productNum : String
    lateinit var productName : TextView
    lateinit var productPrice : TextView
    lateinit var productQuantity : TextView
    lateinit var productDesc : TextView
    lateinit var productImg : ImageView
    lateinit var productCategory : TextView
    lateinit var productPurity : TextView
    var productImgResource : String? = null
    lateinit var fabUpdate : FloatingActionButton
    lateinit var updateFix : LinearLayout
    lateinit var updateDelete : LinearLayout
    lateinit var fabOpen : Animation
    lateinit var fabClose : Animation

    var priceString : String? = null
    var isFabOpen = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_view)
        init()
        setOnClick()

    }


    fun init(){
        productNum = intent.getStringExtra("product_num")
        productName = product_name
        productPrice = product_price
        productQuantity = product_quantity
        productDesc = product_desc
        productImg = product_img
        productCategory = product_category
        productPurity = product_purity
        fabUpdate = product_update
        updateFix = product_update_fix
        updateDelete = product_update_delete

        fabOpen = AnimationUtils.loadAnimation(this@ProductViewActivity,R.anim.fab_open)
        fabClose = AnimationUtils.loadAnimation(this@ProductViewActivity,R.anim.fab_close)


    }

    fun setOnClick(){

        fabUpdate.setOnClickListener {


            anim()
        }
        updateFix.setOnClickListener {
            anim()
            val builder = AlertDialog.Builder(this)
            builder.setTitle("정말 수정하시겠습니까?")
            builder.setPositiveButton("확인") { dialogInterface, i ->
                val intent = Intent(this@ProductViewActivity, ProductUpdateActivity::class.java)
                intent.putExtra("product_num",productNum)
                intent.putExtra("product_name",productName.text)
                intent.putExtra("product_quantity",productQuantity.text)
                intent.putExtra("product_price",priceString)
                intent.putExtra("product_purity",productPurity.text)
                intent.putExtra("product_category",productCategory.text)
                intent.putExtra("product_desc",productDesc.text)
                intent.putExtra("product_img",productImgResource)
                startActivity(intent)
            }
            builder.setNegativeButton("취소"){ dialogInterface, i ->
                dialogInterface.dismiss()
            }
            builder.create().show()
        }
        updateDelete.setOnClickListener{
            anim()
            val builder = AlertDialog.Builder(this)
            builder.setTitle("정말 삭제하시겠습니까?")
            builder.setMessage("삭제한 상품은 되돌릴 수 없습니다.")
            builder.setPositiveButton("확인") { dialogInterface, i ->
                val call = Common.api.deleteProduct(product_num = productNum)
                call.enqueue(object : Callback<ResultResponse>{
                    override fun onResponse(call: Call<ResultResponse>?, response: Response<ResultResponse>?) {
                        if (response != null) {
                            Toast.makeText(this@ProductViewActivity,response.body()?.message,Toast.LENGTH_SHORT).show()
                            if(response.body()!!.success){
                                finish()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResultResponse>?, t: Throwable?) {

                    }

                })
            }
            builder.setNegativeButton("취소"){ dialogInterface, i ->
                dialogInterface.dismiss()
            }
            builder.create().show()
        }
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    fun getData(){
        val call = Common.api.getProduct(product_num = productNum)
        call.enqueue(object : Callback<List<ProductResponse>>{
            override fun onResponse(call: Call<List<ProductResponse>>, response: Response<List<ProductResponse>>) {

                Glide.with(this@ProductViewActivity).load(response.body()?.get(0)?.img).thumbnail(0.1f).into(productImg)
                productImgResource = response.body()?.get(0)?.img
                productName.text = response.body()?.get(0)?.name
                productPrice.text = TextUtil.setPrice(response.body()?.get(0)?.price)+"원"
                priceString = response.body()?.get(0)?.price
                productQuantity.text = response.body()?.get(0)?.quantity
                productDesc.text = response.body()?.get(0)?.desc
                productPurity.text = response.body()?.get(0)?.purity
                productCategory.text = response.body()?.get(0)?.category

            }
            override fun onFailure(call: Call<List<ProductResponse>>, t: Throwable) {
                Log.d("test",t.message)
            }
        })
    }

    fun anim(){
        if(isFabOpen){
            updateFix.startAnimation(fabClose)
            updateDelete.startAnimation(fabClose)
            updateFix.visibility = View.GONE
            updateDelete.visibility = View.GONE
            isFabOpen = false
        } else {
            updateFix.startAnimation(fabOpen)
            updateDelete.startAnimation(fabOpen)
            updateFix.visibility = View.VISIBLE
            updateDelete.visibility = View.VISIBLE
            isFabOpen = true
        }
    }

}
