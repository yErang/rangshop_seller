package com.erang.rangshopseller.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ProgressBar
import com.erang.rangshopseller.R
import kotlinx.android.synthetic.main.activity_loading.*

class LoadingActivity : AppCompatActivity() {

    private lateinit var loadingProgressbar : ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)

        init()
        Thread(Runnable {
            run{
                doWork()
            }
        }).start()
    }

    private fun init(){
        loadingProgressbar = loading_progress
    }

    private fun doWork(){
        for(i in 0..10){
            try {
                Thread.sleep(100)
                loadingProgressbar.setProgress(i)
            }catch(e : Exception) {
                e.printStackTrace()
            }
        }
        val intentMain = Intent(SplashActivity@this, MainActivity::class.java)
        startActivity(intentMain)
        finish()
    }

}
