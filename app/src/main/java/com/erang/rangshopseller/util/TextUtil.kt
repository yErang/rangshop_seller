package com.erang.rangshopseller.util

import android.widget.EditText
import android.widget.TextView
import java.text.DecimalFormat

object TextUtil {

    fun isEmpty(editText: EditText):Boolean{
        return editText.text.toString().isEmpty()
    }

    fun setPrice(priceText: String?) : String? {
        val priceFormat  = DecimalFormat("###,###")
        val formattedPrice = priceFormat.format(priceText?.toInt())

        return formattedPrice
    }


}